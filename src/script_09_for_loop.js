
// example, accessing array elements
var stringArray = new Array("one", "two", "three", "four");
for (var i = 0; i < stringArray.length ; i++)
{
    console.log(stringArray[i]); // one two three four
}
