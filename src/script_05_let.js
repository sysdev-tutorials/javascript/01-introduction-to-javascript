let b = 20;

// re-declaration not allowed
let b = 10; // Error, b is already declared

// update is allowed
b = 10;

function f() {
    if (true) {
        let b = 100; // re-declaration allowed in innner block but value not updated outside the block
        let c = 9
        console.log(c); // 9
    }

    console.log(b); // 10, globally defined value
    console.log(c); // Error, as c is only defined/scoped in if block
}
f()

console.log(b) // 10



