// syntax
// let myFunction = (arg1, arg2, ...argN) => {
//   statement(s)
// }

// using regular function expression
let multiply = function(x, y) {
    return x * y;
}
multiply(2,3); //6

// using arrow function expression
let multiply2 = (x, y) => x * y;
multiply2(2,3); //6


// arrow function with no argument
let greet = () => console.log('Hello');
greet(); // Hello

// arrow function with one argument. note that parenthesis can be omitted
let greet2 = x => console.log(x);
greet2('Hello'); // Hello
