// define a function with a name
function function_name(parameters){
    // body of the function
    console.log("inside function_name")
};
// function call
function_name(); // inside function_name


// define a function and assign to a variable
var function_x = function (parameters){
    // body of the function
    console.log("inside function_x")
};
// function call
function_x(); //inside function


// another example
let square = function(x){
    return (x*x);
};
// function call
console.log(square(9)); // 81


// function as parameters
// define functionB
var functionB = function(){
    return "B";
}
// define functionA which takes function as param
var functionA = function(funcParam) {
    var result = funcParam(); //calling functionB from functionA. Callback pattern!
    console.log(result);
}
// call functionA with functionB as param
functionA(functionB); // B
