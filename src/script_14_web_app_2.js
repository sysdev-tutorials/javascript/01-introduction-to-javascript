// Open the index.html file located in the project root folder in a browser

const message = 'Hello world' // Try edit me

// Update header text
document.querySelector('#header').innerHTML = message
// alternative way of updating header text
//document.getElementById("header").innerHTML = message

const changeBackgroundButton = document.querySelector('#changeBackgroundBtn');
const changeGreetButton = document.querySelector('#changeGreetingBtn');


// 1. lets make the buttons uniform - size, color etc
// uncomment block comment
/*
changeBackgroundButton.classList.add("myButton");
changeGreetButton.classList.add("myButton");
*/


//2. get changeBackgroundBtn element and handle click behavior
// uncomment block comment
/*
changeBackgroundButton.addEventListener('click', function () {
    document.querySelector('body').classList.add("newBackground")
});
*/


//3. get changeGreetingBtn element and handle click behavior
// uncomment block comment
/*
changeGreetButton.addEventListener('click', function () {
    document.querySelector('#header').innerHTML = "Hei there!"
});
*/

