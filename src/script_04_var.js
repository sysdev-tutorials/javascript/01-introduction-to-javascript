// declare variable using var
var a = 10;
// can re-declare same variable using var
var a = 8;
// can update var variable
a = 7;
// scope example for var
var a = 10;
function f(){
    console.log(a); // 10
    a = 20;
}
f();
console.log(a); // 20



