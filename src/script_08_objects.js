var p1 = { name:"Steve Jobs" }; // single key object with literal syntax
var p2 = {firstname: "Steve", lastname: "Jobs"}; // multiple key-value object
console.log(p1.name); // Steve Jobs
console.log(p2.firstname); // Steve

var p3 = new Object(); // Object() constructor function
p3.firstname = "Ole"; // property
p3.lastname = "Nordmann"; // property
console.log(p3.firstname); // Steve


// An object's properties can be accessed using the dot notation or the square brackets obj["property-name"].
// However, method can be invoked only using the dot notation with the parenthesis
var person = {
    firstName: "James",
    lastName: "Bond",
    age: 25,
    getFullName: function () {
        return this.firstName + ' ' + this.lastName; // this represents this object
    }
};
person.firstName; // James
person.lastName; // Bond
person["firstName"];// James
person["lastName"];// Bond
person.getFullName(); // calling getFullName function and returns 'James Bond'


// Note. Objects are more useful when dealing with data, for example JSON data.
