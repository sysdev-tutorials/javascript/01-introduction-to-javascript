
//Arithmetic
var x = 5, y = 10;
var z = x + y; // 15
z = y - x; // 5
z = x * y; // 50
z = y / x; // 2
z = x % 2; // 1

var a = "Hello", b = "World!";
x + a; //returns 5Hello
a + b; // HelloWorld!
a + true; // Hellotrue
a - b; // NaN; - operator can only used with numbers


// Comparision
var a = 5, b = 10, c = "5";
var x = a;

a == c; // true, compares ony value not type
a === c; // false, compared both value and type
a == x; // true
a != b; // true
a > b; // false
a < b; // true
a >= b; // false
a <= b; // true


// logical operators &&, ||, !
var a = 5, b = 10;
(a != b) && (a < b); // true
(a > b) || (a == b); // false
(a < b) || (a == b); // true
!(a < b); // false
!(a > b); // true

// assignment operators =, +=. -=, *=, /=, %=
var x = 5, y = 10, z = 15;
x = y; //x would be 10
x += 1; //x would be 6
x -= 1; //x would be 4
x *= 5; //x would be 25
x /= 5; //x would be 1
x %= 2; //x would be 1

// ternary operator, condition ? a : b
var a = 10, b = 5;
var c = a > b? a : b; // value of c would be 10
var d = a > b? b : a; // value of d would be 5
