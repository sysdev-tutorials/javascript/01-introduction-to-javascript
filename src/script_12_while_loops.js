// while
let n = 0;
while (n < 3) {
    n++;
}
console.log(n); // 3


// do...while
let result = '';
let i = 0;
do {
    i = i + 1;
    result = result + i;
} while (i < 5);

console.log(result); // 12345