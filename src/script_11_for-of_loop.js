// The for-of loop loops over an iterable

//for (variable of iterable) {
//    statement
// }

// iterables can be array objects or custom made iterables

// example
var stringArray = new Array("one", "two", "three", "four");
for (var prop of stringArray)
{
    console.log(prop); // one, two, three, four
}
