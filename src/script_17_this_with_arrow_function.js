// arrow function defined in a global scope
let arrowFunction = () => {
    //console.log(this == window); //true, the arrow function present or defined in window
    console.log(this.age); // undefined, age not present in window
}
arrowFunction();


// arrow function defined in a regular function
var age1 = 20;
let function2 = function(){
    //console.log(this == window); //true, the regular function called by window

    var age2 = 25;
    let arrowFunction2 = () => {
        //console.log(this == window); // arrowFunction2 is considered defined in window
        console.log(this.age1); //20, age1 present in window
        console.log(this.age2); // undefind, age2 is not present in window
    }
    arrowFunction2();

}
function2();


// arrow function as JavaScript object method. NOT SO USEFUL!
// Note that arrow functions are not suited for object methods!!
const person = {
    firstName: "Ole",
    lastName: "Nordmann",
    id: 1234,
    fullName: function () {
        // console.log(this == person);// true, regular function called by the person object
        return this.firstName + " " + this.lastName;
    },
    fullName1: () => {
        // console.log(this == window); //true, this ínherits from parent scope i.e the person is defined in window
        return this.firstName + " " + this.lastName;
    }
}
console.log(person.fullName()); // Ole Nordmann
console.log(person.fullName1()); // undefined undefined



// arrow function can be useful when they are used inside JavaScript object´s regular method
const person2 = {
    firstName: "Ole",
    lastName: "Nordmann",
    id: 1234,
    fullName: function () {
        //at this point, this == person2

        // arrow func inside objects regular function
        var innerArrowFunc = () => {
            // at this point also, this == person2
            console.log(this == person2); //true, innerArrowFunc is defined in person object scope
            console.log(this.firstName + " " + this.lastName); // Ole Nordmann
        }
        innerArrowFunc(); // Ole Nordmann
    },
}
person2.fullName(); // Ole Nordmann

