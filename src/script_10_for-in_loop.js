// loops over an object’s elements one by one.

// for (variable in object)
//   statement

// example
var person = {
    firstName: "James",
    lastName: "Bond",
    age: 25,
    getFullName: function () {
        return this.firstName + ' ' + this.lastName; // this represents this object
    }
};

for (var prop in person)
{
    console.log(prop); // firstName, lastName, age, getFullName
    console.log(person[prop]); // James Bond 25 ...
}
