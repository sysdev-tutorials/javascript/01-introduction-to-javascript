// array literal example
var stringArray = ["one", "two", "three"];
var numericArray = [1, 2, 3, 4];
var decimalArray = [1.1, 1.2, 1.3];
var booleanArray = [true, false, false, true];
var mixedArray = [1, "two", "three", 4];

// array constructor examples
var stringArray = new Array();
stringArray[0] = "one";
stringArray[1] = "two";
stringArray[2] = "three";
stringArray[3] = "four";

var numericArray = new Array(3);
numericArray[0] = 1;
numericArray[1] = 2;
numericArray[2] = 3;

// array defined with indexed
var mixedArray = new Array(1, "two", 3, "four");
console.log(mixedArray[0]) // 1
console.log(mixedArray[1]) // two
console.log(mixedArray[2]); // 3
console.log(mixedArray["two"]); // undefined

// access array elements.
// NOTE here that array elements are not accessible via indexes in this case
// because elements are not initialized per index based
// this approach is technically possible not recommend when you work with Arrays.
var stringArray1 = new Array();
stringArray1["one"] = "one";
stringArray1["two"] = "two";
console.log(stringArray1["one"]) // one
console.log(stringArray1[0]) // undefined

// access array elements
var stringArray2 = new Array("one", "two", "three", "four");
stringArray2[0]; // "one"
stringArray2[1]; // "two"
console.log(stringArray2[2]) // three
console.log(stringArray2["one"]) // undefined
