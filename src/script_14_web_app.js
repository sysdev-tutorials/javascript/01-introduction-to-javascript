// Open the index.html file located in the project root folder in a browser


const message = 'Hello world' // Try edit me

// Update header text
document.querySelector('#header').innerHTML = message

// alternative way of updating header text
//document.getElementById("header").innerHTML = message

// Log to console
console.log(message)