var b = 20;

// Simple invocation
function myFunction() {
  var a = 10;
  console.log(this == window); // true, window is calling the method
  console.log(this.a); // undefined
  console.log(this.b); // 20
}
myFunction(); // function is used globally (window)

// regular nested function usually have this as window
function myFunction() {
  var innerFunc = function() {
  	console.log(this == window); // true
  }
  innerFunc();
}
myFunction(); // function is used globally (window)


// Object method invocation
const myObject = {
    a: "123",
    method() {
        console.log(this == myObject); //true, the value of this is the object calling the method i.e. myObject
        console.log(this.a); // logs 123
    }
};
myObject.method(); // i.e function is executed by the myObject


// constructor invocation
function MyFunction() {
  var x = 5;
  console.log(this); // the value this equals to the newly created instance
  console.log(this.x); // x is not accessible in the newly create instance
}
new MyFunction(); // i.e function is executed by new instance of MyFunction is created
