var name = "Connie Client";

var fName = name.substring(0, name.indexOf(" ")); // "Connie"

var len = name.length; // 13

var anotherName = 'Melvin Merchant'; // can use "" or ' ' to represent a string

var boolVar = false;  // false

var numVar = 10; // value 10, type number

var strVar = '10'; // value 10, type string

var x; // value undefined, type undefined

var y = null; // value null, type object

console.log(z); // undefined, variable is not declared yet
