# 01 Introduction to JavaScript

## Intro/History
JavaScript started as a client-side programming language in early days. But these days besides client side applications, it is well used to build server side applications as wells for building cross-platform mobile applications.

In the context of this course, we will however focus JavaScript on the client side use case of web-applications development!

Fundamentally, there are three pillers when it comes to the client side (or frontend side) of any web application. These are following

- HTML: It provides the basic structure of a page or an application. Various HTML element types are used for that, for example ```<p>, <botton>, <head>, <body>, <label>, <table>```, etc. More here https://developer.mozilla.org/en-US/docs/Web/HTML/Element
- CSS: It controls the layout, format and presentation of the HTML elements. More here https://developer.mozilla.org/en-US/docs/Web/CSS
- JavaScript: It adds dynamic behavior or interaction to the page. For example allows users to interact and control the behavior of elements.

So, HTML is the ```markup language``` and CSS is the ```design language```, then JavaScript is the ```programming language```.

When a webpage is loaded, the browser will generate 'Document Object Model (HTML-DOM)'. Inspect a page using developers tools in a browser. Note that the generated HTML-DOM has embeded ```script```tags and these contain scripts. The scripts are used to manipulate (add/edit/update in various aspects) the HTML-DOM i.e the page.

It is important to note that HTML-DOMs are APIs and allow to add, remote and edit HTML elements, attributes, CSS styles, events and so on. This is where JavaScript comes into play! It can be used to manipulate HTML-DOM thereby turning static websites into dynamic ones. 

Note that using JavaScript you can not only manipulate HTML-DOM but also do regular programming tasks such as actions, conditions, calculations, network requests, concurrent tasks and so on.

Some examples of how the DOM elements can be queried and updated are following!

- document.querySelector()
- document.getElementById()
- document.getElementsByClassName()
- document.getElementsByTagName()

Let´s say we have a following HTML element in a html file.
```
<h1 id="header"></h1>
```

One can programmatically change the value for that ```<h1>``` element to 'Hello World' text as following. Note here that the ```document``` variable is a global variable representing the current page you are viewing

```
document.querySelector('#header').innerHTML = "Hello world"
```

More on DOM document API methods here https://developer.mozilla.org/en-US/docs/Web/API/Document



## Variables

JavaScript is considered a "weakly typed" or "dynamically typed" or "untyped" language.

Variables are declared with ```var, let``` and ```const``` keywoards (case sensitive).

Types are not specified to the variable but JavaScript does have following loosely coupled types - ```Number, Boolean, String, Array, Object, Function, Undefined```

One can find out the variable´s type by calling ```typeof```

```
var name = "Connie Client";

var fName = name.substring(0, name.indexOf(" ")); // "Connie"

var len = name.length; // 13

var anotherName = 'Melvin Merchant'; // can use "" or ' ' to represent a string

var boolVar = false;  // false

var numVar = 10; // value 10, type number, JavaScript does not define different types of numbers

var strVar = '10'; // value 10, type string

var x; // value undefined, type undefined

var y = null; // value null, type object

console.log(z); // undefined, variable is not declared yet
```

## Operators
JavaScript includes operators same as other languages.

```Arithmetic operators +, -, *, /, %, ++, --``` are used to perform mathematical operations between numeric operands.

Note that the ```+``` operator performs concatenation operation when one of the operands is of string type. The ```-``` operator can only used with numbers.

```
var x = 5, y = 10;
var z = x + y; // 15
z = y - x; // 5
z = x * y; // 50
z = y / x; // 2
z = x % 2; // 1

var a = "Hello", b = "World!";
x + a; //returns 5Hello
a + b; // HelloWorld!
a + true; // Hellotrue
a - b; // NaN; - operator can only used with numbers
```

```Comparison operators ==, ===, !=, >, <, >=, <=``` that compare two operands and return a boolean value ```true``` or ```false```.

```
var a = 5, b = 10, c = "5";
var x = a;

a == c; // true, compares ony value not type
a === c; // false, compared both value and type
a == x; // true
a != b; // true
a > b; // false
a < b; // true
a >= b; // false
a <= b; // true
```

```Logical , Assignment and Ternary operators``` work similar to other programming languages such as Java.

```
// logical operators &&, ||, !
var a = 5, b = 10;
(a != b) && (a < b); // true
(a > b) || (a == b); // false
(a < b) || (a == b); // true
!(a < b); // false
!(a > b); // true

// assignment operators =, +=. -=, *=, /=, %=
var x = 5, y = 10, z = 15;
x = y; //x would be 10
x += 1; //x would be 6
x -= 1; //x would be 4
x *= 5; //x would be 25
x /= 5; //x would be 1
x %= 2; //x would be 1

// ternary operator, condition ? a : b
var a = 10, b = 5;
var c = a > b? a : b; // value of c would be 10
var d = a > b? b : a; // value of d would be 5
```



## Functions

A function can be defined with a function name.

```
// define a function with a name
function function_name(parameters){
    // body of the function
    console.log("inside function_name")
};

// function call
function_name(); // inside function_name

```

A function can also be defined as annonymous and assigned to a variable

```
// define a function and assign to a variable
var function_x = function (parameters){
    // body of the function
    console.log("inside function_x")
};

// function call
function_x(); //inside function


// another example
let square = function(x){
    return (x*x);
};
// function call
console.log(square(9)); // 81
```

Functions can be passed as parameter another function

```
// define functionB
var functionB = function() {
    return "B";
}

// define functionA which takes function as param
var functionA = function(funcParam) {
    var result = funcParam(); //calling functionB from functionA. Callback pattern!
    console.log(result); 
}

// call functionA with functionB as param
functionA(functionB); // B
```


### Difference between var, let and const keywords

```var```: User can re-declare variable using var and user can update var variable.

The scope of the var keyword is the global or function scope. It means variables defined outside the function can be accessed globally, and variables defined inside a particular function can be accessed within the function.

```
// declare variable using var
var a = 10; 
    
// can re-declare same variable using var
var a = 8; 
    
// can update var variable
a = 7;

// scope example for var
var a = 10;
function f(){
    console.log(a); // 10
    a = 20;
}
f(); 
console.log(a); // 20
```

```let``` keyword is an improved version of the var keyword. The scope of a let variable is only block scoped. It can’t be accessible outside the particular block (```{}```).

Users cannot re-declare the variable defined with the let keyword but can update it.

```
let b = 20;

// re-declaration not allowed
let b = 10; // Error, b is already declared
 
// update is allowed
b = 10;

function f() {
    if (true) {
        let b = 100; // re-declaration allowed in innner block but value not updated outside the block
        let c = 9
        console.log(c); // 9
    }

    console.log(c); // Error, as c is defined in if block
}
f()
 
console.log(b) // 10
```


```const``` keyword has all the properties that are the same as the let keyword, except the user cannot update it. That means it is the most strict form of variable declaration!


```
const d = 10;
d = 20; // Error, update is not allowed

function f() {
    const d = 40; // 40, re-declaration allowed in innner block block but value not updated outside the block
    console.log(d)
}
f();

console.log(d); // 10
```

More, check  https://www.geeksforgeeks.org/difference-between-var-let-and-const-keywords-in-javascript/


## Arrays
An array in JavaScript can be defined and initialized in two ways, ```array literal``` and ```array constructor``` syntax.

Please note that array can only have numeric index (key). Index cannot be of string or any other data type.

```
// array literal example
var stringArray = ["one", "two", "three"];
var numericArray = [1, 2, 3, 4];
var decimalArray = [1.1, 1.2, 1.3];
var booleanArray = [true, false, false, true];
var mixedArray = [1, "two", "three", 4];


// array constructor examples
var stringArray = new Array();
stringArray[0] = "one";
stringArray[1] = "two";
stringArray[2] = "three";
stringArray[3] = "four";

var numericArray = new Array(3);
numericArray[0] = 1;
numericArray[1] = 2;
numericArray[2] = 3;

var mixedArray = new Array(1, "two", 3, "four"); // unlike in Java
```

An array elements (values) can be accessed using index (key). Specify an index in square bracket with array name to access the element at particular index. Note that index of an array starts from zero.

```
var stringArray = new Array("one", "two", "three", "four");
stringArray[0]; // "one"
stringArray[1]; // "two"
```


## Objects
In JavaScript, an object can be created in two ways:
- using Object Literal/Initializer Syntax
- using the Object() Constructor function with the ```new``` keyword

Unlike Array, an object is like a Map in Java where Keys are mapped to Values.

Note that Objects can be declared as variables.

```
var p1 = { name:"Steve Jobs" }; // single key object with literal syntax
var p2 = {firstname: "Steve", lastname: "Jobs"}; // multiple key-value object 
console.log(p1.name); // Steve Jobs
console.log(p2.firstname); // Steve

var p3 = new Object(); // Object() constructor function
p3.firstname = "Ole"; // property
p3.lastname = "Nordmann"; // property
```

An object's properties can be accessed using the dot notation obj.property-name or the square brackets obj["property-name"]. However, method can be invoked only using the dot notation with the parenthesis, obj.method-name(), as shown below.


```
var person = { 
    firstName: "James", 
    lastName: "Bond", 
    age: 25, 
    getFullName: function () { 
        return this.firstName + ' ' + this.lastName; // this represents this object
    } 
};

person.firstName; // James
person.lastName; // Bond
person["firstName"];// James
person["lastName"];// Bond
person.getFullName(); // calling getFullName function and returns 'James Bond'
```


## Control statements (if-else, loops, etc)

```For loop``` is similar to other programming languages such as Java.

```
for(initializer; condition; iteration)
{
    // Code to be executed
}

// example, accessing array elements
var stringArray = new Array("one", "two", "three", "four");
for (var i = 0; i < stringArray.length ; i++) 
{
    console.log(stringArray[i]);
}

```

The ```for-in loop``` loops over an object’s elements one by one. The for-in loop returns keys or indexes of the object.

```
for (variable in object)
  statement

// example
var person = { 
  firstName: "James", 
  lastName: "Bond", 
  age: 25, 
  getFullName: function () { 
    return this.firstName + ' ' + this.lastName; // this represents this object
    } 
};

for (var prop in person) 
{
    console.log(prop); // firstName, lastName, age, getFullName
    //console.log(person[prop]); // check what is printed??
}

```

The ```for-of loop``` loops over an iterable (i.e over an array) one by one. The for-of loop returns values, not keys or indexes.

```
for (variable of iterable) {
  statement
}

// example
var stringArray = new Array("one", "two", "three", "four");
for (var prop of stringArray) 
{
    console.log(prop); // one, two, three, four
}
```

```while``` and ```do-while``` loops are similar to other programming languages such as Java.

```
// while toop syntax
while (condition) {
    // Statements
}

// do-while loop syntax
do {
    // Statements
} while (condition);
```

Note that there are two ways to ignore loop in else condition: ```continue``` and ```break```.


## Arrow function

Arrow function, introduced in the ES6 version of JavaScript,allows you to create functions in a cleaner way compared to regular functions.

```
// using regular function expression
let x = function(x, y) {
   return x * y;
}

// using arrow function expression
let x = (x, y) => x * y;
```

The syntax for arrow function is as follows

```
let myFunction = (arg1, arg2, ...argN) => {
    statement(s)
}

// example
let sum = (a, b) => {
    let result = a + b;
    return result;
}
let result1 = sum(5,7);
console.log(result1); // 12
```

If the body has single statement or expression, you can write arrow function as:

```
let myFunction = (arg1, arg2, ...argN) => expression
```

Arrow Function with no argument
```
let greet = () => console.log('Hello');
greet(); // Hello
```

Arrow function with one argument
```
let greet = x => console.log(x);
greet('Hello'); // Hello 
```

## Using ```this``` value

```this``` represents an object that executes the current function. 
In short, this is defined by the function execution context. 
For example, it refers to a global object window when a function is being executed from a global object.

Therefore the behavior of ```this``` differs when it used with regular functions compared to when it is used with arrow functions.

### ```this``` value with regular functions
Inside regular functions, ```this``` represents the execution context which is usually dynamic. 
That means that the value of this depends on how the function is invoked.
This refers to the context of the function being called which is the ```parent``` object.

There are multiple ways you can invoke a regular function

```
// Simple invocation
function myFunction() {
  console.log(this); // value of this equals to the global object
}
myFunction(); // function is used globally (window)
```

```
// Object method invocation 
const myObject = {
    a: "b",
    method() {
        console.log(this); // the value of this is the object owning the method i.e. myObject
        console.log(this.a); // logs b
    }
};
myObject.method(); // i.e function is executed by the myObject
```

```
// constructor invocation
function MyFunction() {
  var x = 5;
  console.log(this); // the value this equals to the newly created instance
  console.log(this.x); // x is not accessible in the newly create instance
}
new MyFunction(); // i.e function is executed by new instance of MyFunction
```

More here https://www.codementor.io/@dariogarciamoya/understanding--this--in-javascript-du1084lyn


### ```this``` value with arrow functions
Arrow functions treat this keyword differently. Arrow functions don’t have their own this context.

When used inside the outer (enclosing) function, this keyword will point to where the function is present (or defined) and it has nothing to do with the caller (i.e the caller object). It refers the scope where the function is present i.e. the ```parent´s scope``` or ```enclosing context```, which is also called ```lexical scoping```.

Some examples,

```
// arrow function defined in a global scope
let arrowFunction = () => {
    //console.log(this == window); //true, the arrow function present or defined in window
    console.log(this.age); // undefined, age not present in window
}
arrowFunction();


// arrow function defined in a regular function
var age1 = 20;
let function2 = function(){
    //console.log(this == window); //true, the regular function called by window

    var age2 = 25;
    let arrowFunction2 = () => {
        //console.log(this == window); // arrowFunction2 is considered defined in window
        console.log(this.age1); //20, age1 present in window
        console.log(this.age2); // undefind, age2 is not present in window
    }
    arrowFunction2();

}
function2();


// arrow function as JavaScript object method. NOT SO USEFUL!
// Note that arrow functions are not suited for object methods!!
const person = {
    firstName: "Ole",
    lastName: "Nordmann",
    id: 1234,
    fullName: function () {
        // console.log(this == person);// true, regular function called by the person object
        return this.firstName + " " + this.lastName;
    },
    fullName1: () => {
        // console.log(this == window); //true, this ínherits from parent scope i.e the person is defined in window
        return this.firstName + " " + this.lastName;
    }
}
console.log(person.fullName()); // Ole Nordmann
console.log(person.fullName1()); // undefined undefined



// arrow function can be useful when they are used inside JavaScript object´s regular method
const person2 = {
    firstName: "Ole",
    lastName: "Nordmann",
    id: 1234,
    fullName: function () {
        //at this point, this == person2

        // arrow func inside objects regular function
        var innerArrowFunc = () => {
            // at this point also, this == person2
            console.log(this == person2); //true, innerArrowFunc is defined in person object scope
            console.log(this.firstName + " " + this.lastName); // Ole Nordmann
        }
        innerArrowFunc(); // Ole Nordmann
    },
}
person2.fullName(); // Ole Nordmann
```

More references on arrow function and this
- https://www.freecodecamp.org/news/javascript-lexical-scope-tutorial/
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions
- https://www.codementor.io/@dariogarciamoya/understanding-this-in-javascript-with-arrow-functions-gcpjwfyuc
- https://www.programiz.com/javascript/arrow-function



## IDE - IntelliJ, VSCode, or browser based IDE like GitPod, etc
